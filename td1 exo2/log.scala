import scala.io.Source
import scala.collection.mutable.ListBuffer



val filename = "logs.txt"
var timeStamps=new ListBuffer[Int]()
var firstHosts=new ListBuffer[String]()
var secondHosts=new ListBuffer[String]()
var con = new Connection
println("----------All connections----------")
for (line <- Source.fromFile(filename).getLines) {
	//récupère les lignes du fichier
  println("Connection "+line)
  var splitted=line.split(" ")
  timeStamps+=splitted(0).toInt
  firstHosts+=splitted(1)
  secondHosts+=splitted(2)
  con.timestamp=splitted(0).toInt
  con.host1=splitted(1)
  con.host2=splitted(2)
}

// lastConnection
println("----------Last connection----------")
println("Last Connexion at : "+timeStamps.max+" by "+firstHosts(timeStamps.indexOf(timeStamps.max)))

//max of connexions
val map = firstHosts.groupBy(identity).mapValues(_.size)
println("----------Max connections----------")
println(map.maxBy { case (key, value) => value })